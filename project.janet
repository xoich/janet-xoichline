(declare-project
  :name "janet-xoichline"
  :author "xoich"
  :license "MIT")

(declare-native
  :name "xoichline"
  :source ["main.c" "bestline/bestline.c"])
