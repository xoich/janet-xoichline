#include <janet.h>
#include "bestline/bestline.h"

static Janet cfun_readline(int32_t argc, Janet *argv) {
    janet_arity(argc, 0, 2);
    const char* prompt = janet_optcstring(argv, argc, 0, "> ");
    const char* init = janet_optcstring(argv, argc, 1, "");
    char* line = bestlineInit(prompt, init);
    if (line == NULL) {
        return janet_wrap_nil();
    }
    Janet string = janet_cstringv(line);
    free(line);
    return string;
}

static const JanetReg cfuns[] = {
    {"readline", cfun_readline, "(xoichline/readline &opt prompt init)\n\nRead a line with xoichline."},
    {NULL, NULL, NULL}
};

JANET_MODULE_ENTRY(JanetTable *env) {
    janet_cfuns(env, "xoichline", cfuns);
}
